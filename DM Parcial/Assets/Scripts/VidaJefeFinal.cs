using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaJefeFinal : MonoBehaviour
{
    public Image barradeVida;
    public Image fondoBarraVida;
    public float VidaActual;
    public float vidaMaxima;
    public AudioSource ExplosionAudio;
    public GameObject LevelDoneScreen;
    public AudioSource MusicaFondo;
    public UIQuitarVidaJefeFInal UILastimado;
    // Start is called before the first frame update
    void Start()
    {
        vidaMaxima = 100f;
        VidaActual = 100f;
    }

    // Update is called once per frame
    void Update()
    {
        barradeVida.fillAmount = VidaActual / vidaMaxima;
        if(VidaActual <= 0)
        {
            Time.timeScale = 0;
            LevelDoneScreen.SetActive(true);
            MusicaFondo.Stop();
            barradeVida.gameObject.SetActive(false);
            fondoBarraVida.gameObject.SetActive(false);
        }
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            
            collision.gameObject.SetActive(false);
            VidaActual = VidaActual - 5;
            ExplosionAudio.Play();
            UILastimado.EstaHeridoVerdadero();


        }
    }
}
