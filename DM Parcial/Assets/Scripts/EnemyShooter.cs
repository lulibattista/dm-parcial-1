using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooter : MonoBehaviour
{
    public Transform player; // Asigna el jugador desde el editor
    public float detectionRange = 50f; // Rango de detecci�n del enemigo
    public float fireRate = 5f; // Tasa de disparo en segundos
    public BulletPoolEnemigo bulletPool; // Referencia al script del pool de balas

    private float nextFireTime;

    void Start()
    {
        // Encuentra al jugador por tag si no se ha asignado
        if (player == null)
        {
            GameObject playerObj = GameObject.FindGameObjectWithTag("Nave");
            if (playerObj != null)
            {
                player = playerObj.transform;
            }
        }

        nextFireTime = Time.time;
    }

    void Update()
    {
        if (player == null) return;

        // Comprueba la distancia entre el enemigo y el jugador
        if (Vector3.Distance(transform.position, player.position) <= detectionRange)
        {
            // Si el enemigo puede disparar (basado en la tasa de disparo)
            if (Time.time >= nextFireTime)
            {
                
                FireProjectile();
                nextFireTime = Time.time + fireRate;
            }
        }
    }

    void FireProjectile()
    {
        // Llama al m�todo del BulletPool para disparar una bala desde la posici�n del enemigo
        bulletPool.ShootFromPool(transform.position, transform.rotation);
        

    }

    void OnDrawGizmosSelected()
    {
        // Dibuja el rango de detecci�n en la escena para visualizaci�n
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRange);
    }
}

