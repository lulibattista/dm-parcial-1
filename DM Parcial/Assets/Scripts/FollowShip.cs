using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowShip : MonoBehaviour
{
    public Transform target; // El objeto que quieres seguir
    public float smoothness = 0.2f; // La suavidad del seguimiento (valores m�s bajos hacen que el seguimiento sea m�s lento)
    public AudioSource ExplosionAudio;

    public GameObject explosionAlienPrefab;
    void Update()
    {
        if (target != null)
        {
            // Calcula la nueva posici�n interpolando entre la posici�n actual y la posici�n del objetivo
            Vector3 newPosition = Vector3.Lerp(transform.position, target.position, smoothness * Time.deltaTime);

            // Actualiza la posici�n del objeto que sigue
            transform.position = newPosition;
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        // Comprueba si la colisi�n involucra al GameObject que quieres desactivar
        if (collision.gameObject.CompareTag("Bala"))
        {
            // Desactiva el GameObject
            this.gameObject.SetActive(false);
            collision.gameObject.SetActive(false);
            ExplosionAudio.Play();
            Instantiate(explosionAlienPrefab, collision.transform.position, Quaternion.identity);
        }
        
        if (collision.gameObject.CompareTag("Nave"))
        {
            this.gameObject.SetActive(false);
            ExplosionAudio.Play();
            Instantiate(explosionAlienPrefab, collision.transform.position, Quaternion.identity);
        }


    }
}
