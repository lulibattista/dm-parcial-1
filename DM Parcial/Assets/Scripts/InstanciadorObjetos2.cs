using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorObjetos2 : MonoBehaviour
{
    public List<GameObject> ObjetosEspaciales;
    public float respawningTimer;
    public GameObject instantiatePos;
    private float time = 0;
    public GameObject Starpos;
    public GameObject Target;

    // Start is called before the first frame update
    void Start()
    {
        EstrellaController.StarVelocity = 8f;
        AsteroideControl2.SuperAsteroideVelocity = 8f;
        OmegaAsteroideControl.OmegaAsteroideVelocity = 8f;
        AsteroideControl.AsteroideVelocity = 8f;
        CrystalController.CrystalVelocity = 8f;
    }

    // Update is called once per frame
    void Update()
    {
        SpawnObjects();
        ChangeVelocity();
    }

    private void SpawnObjects()
    {
        int numeroobject;
        Vector3 posicion;
        int posrnd;
        respawningTimer -= Time.deltaTime;


        if (respawningTimer <= 0)
        {

            //Instantiate(ObjetosEspaciales[UnityEngine.Random.Range(0, ObjetosEspaciales.Count)], instantiatePos.transform.position, instantiatePos.transform.rotation).gameObject.SetActive(true);
            numeroobject = UnityEngine.Random.Range(0, ObjetosEspaciales.Count);
            if (numeroobject == 0)
            {
                Instantiate(ObjetosEspaciales[numeroobject], Starpos.transform).gameObject.SetActive(true);
            }
            else
            {
                posrnd = UnityEngine.Random.Range(0, 10);
                posicion = new Vector3(posrnd - 1, 0f, 40f);


                Instantiate(ObjetosEspaciales[numeroobject], posicion, instantiatePos.transform.rotation).gameObject.SetActive(true);
            }
            respawningTimer = UnityEngine.Random.Range(1, 1);
        }

    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        
    }
}
