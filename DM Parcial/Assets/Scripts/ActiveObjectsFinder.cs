using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveObjectsFinder : MonoBehaviour
{
    void Update()
    {
        // Encuentra todos los objetos activos en la escena
        GameObject[] allActiveObjects = FindObjectsOfType<GameObject>();

        // Recorre cada objeto activo
        foreach (GameObject obj in allActiveObjects)
        {
            if(obj.tag == "Asteroide" || obj.tag == "Alien" || obj.tag == "Crystal")
            {
                if(obj.transform.position.z < -14.5f)
                {
                    Destroy(obj);
                }
            }
            
        }

    }
}
