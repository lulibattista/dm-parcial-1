using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorObjetos : MonoBehaviour
{
    public List<GameObject> ObjetosEspaciales;
    public float respawningTimer;
    public GameObject instantiatePos;
    private float time = 0;
    public GameObject Starpos;
    public GameObject Target;
    public GameObject NaveEnemigaRot;

    // Start is called before the first frame update
    void Start()
    {
        EstrellaController.StarVelocity = 10f;
        SateliteController.SateliteVelocity = 10f;
        CrystalController.CrystalVelocity = 10f;
        AsteroideControl.AsteroideVelocity = 10f;
        AsteroideControl2.SuperAsteroideVelocity = 20f;
        OmegaAsteroideControl.OmegaAsteroideVelocity = 10f;
    }

    // Update is called once per frame
    void Update()
    {
        SpawnObjects();
        ChangeVelocity();
    }

    private void SpawnObjects()
    {
        int numeroobject;
        Vector3 posicion;
        int posrnd;
        respawningTimer -= Time.deltaTime;
        

        if (respawningTimer <= 0)
        {
            
            //Instantiate(ObjetosEspaciales[UnityEngine.Random.Range(0, ObjetosEspaciales.Count)], instantiatePos.transform.position, instantiatePos.transform.rotation).gameObject.SetActive(true);
            numeroobject = UnityEngine.Random.Range(0, ObjetosEspaciales.Count);
            if(numeroobject == 0)
            {
                Instantiate(ObjetosEspaciales[numeroobject],Starpos.transform).gameObject.SetActive(true);
            }
            else if(numeroobject == 5)
            {
                posrnd = UnityEngine.Random.Range(0, 10);
                posicion = new Vector3(posrnd - 1f, 0f, 40f);
                ObjetosEspaciales[numeroobject].transform.localScale = new Vector3(0.126662f, 0.126662f, 0.126662f);
                
                Instantiate(ObjetosEspaciales[numeroobject], posicion, NaveEnemigaRot.transform.rotation).gameObject.SetActive(true);
                //instancia.transform.localScale =new Vector3(0.5280665f, 0.5280665f, 0.5280665f), ;
            }
            else
            {
                posrnd = UnityEngine.Random.Range(0,10);
                posicion = new Vector3(posrnd - 1, 0f, 40f);
               
                
                Instantiate(ObjetosEspaciales[numeroobject], posicion, instantiatePos.transform.rotation).gameObject.SetActive(true);
            }
            respawningTimer = UnityEngine.Random.Range(2, 6);
        }

    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        
    }
}
