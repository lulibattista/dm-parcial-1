using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstrellaController : MonoBehaviour
{
    public static float StarVelocity;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(StarVelocity * Time.deltaTime, 0f, 0f);
    }
}
