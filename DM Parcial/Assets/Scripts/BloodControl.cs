using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloodControl : MonoBehaviour
{
    public Image BloodEffectImage;
    private float r;
    private float g;
    private float b;
    private float a;
    public static bool EstaHerido;
    public int VariableContador;

    void Start()
    {
        r = BloodEffectImage.color.r;
        g = BloodEffectImage.color.g;
        b = BloodEffectImage.color.b;
        a = BloodEffectImage.color.a;
        EstaHerido = false;
    }

    
    void Update()
    {
        if (EstaHerido)
        {
            a += 0.01f;
            VariableContador -= 1;
        }
        a -= 0.001f;
        a = Mathf.Clamp(a, 0, 1f);
        ChangeColor();
        if(VariableContador <= 0)
        {
            EstaHerido = false;
        }
    }

    private void ChangeColor()
    {
        Color c = new Color(r, g, b, a);
        BloodEffectImage.color = c;
    }

    public void EstaHeridoVerdadero()
    {
        EstaHerido = true;
        VariableContador = 30;
    }
}
