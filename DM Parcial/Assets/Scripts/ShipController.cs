using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    public BulletPool bulletPool;
    public GameObject instantiatePos;
    public Rigidbody naveRigid;
    public AudioSource LaserAudio;

    
    void Update()
    {
        Vector3 posicionActual;
        posicionActual = naveRigid.position;

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Shoot();
            LaserAudio.Play();
        }
        
        if (FueradeLimiteDerecho())
        {
            
            posicionActual.x = 3.5f;
            //Debug.Log("" + posicionActual);
            naveRigid.transform.position= posicionActual;

        }

        if (FueradeLimiteIzquierda())
        {
            posicionActual.x = -4.04f;
            naveRigid.transform.position = posicionActual;
        }

        if (FueraLimiteAtras())
        {
            posicionActual.z = -6.63f;
            naveRigid.transform.position = posicionActual;
        }
    }

    void Shoot()
    {
        Vector3 spawnPosition = transform.position; //+ transform.forward * 2.0f; // Ajusta la distancia hacia adelante seg�n tu dise�o
        //Quaternion spawnRotation = transform.rotation;
        bulletPool.ShootFromPool(spawnPosition, instantiatePos.transform.rotation);
    }

    public bool FueradeLimiteDerecho()
    {
        if(naveRigid.position.x > 3.5f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool FueradeLimiteIzquierda()
    {
        if(naveRigid.position.x < -4.04f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool FueraLimiteAtras()
    {
        if(naveRigid.position.z < -6.63f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


}
