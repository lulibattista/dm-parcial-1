using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesControl : MonoBehaviour
{
    public GameObject Vida1;
    public GameObject Vida2;
    public GameObject Vida3;
    public GameObject EscudoImage;
    public int vidas = 3;
    public bool Escudo;
    public Game_Hud GameBoss;
    public AudioSource Shieldup;
    public AudioSource EstrellaPowerup;
    public AudioSource AsteroideClash;
    public AudioSource GameOverAudio;
    public AudioSource SonidoSatelite;
    public AudioSource SonidoNaveEnemiga;
    public BloodControl HeridaStatus;

    // Variables para los prefabs de explosión
    public GameObject explosionAlienPrefab;
    public GameObject explosionAsteroidePrefab;
    public GameObject explosionEstrellaPrefab;
    public GameObject explosionBalaEnemigoPrefab;
    public GameObject explosionSatelitePrefab;
    public GameObject explosionSuperAsteroidePrefab;
    public GameObject explosionOmegaAsteroidePrefab;
    public GameObject explosionCrystalPrefab;

    // Start is called before the first frame update
    void Start()
    {
        Escudo = false;
        EscudoImage.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (vidas == 3)
        {
            Vida1.gameObject.SetActive(true);
            Vida2.gameObject.SetActive(true);
            Vida3.gameObject.SetActive(true);
        }
        else if (vidas == 2)
        {
            Vida1.gameObject.SetActive(true);
            Vida2.gameObject.SetActive(true);
            Vida3.gameObject.SetActive(false);
        }
        else if (vidas == 1)
        {
            Vida1.gameObject.SetActive(true);
            Vida2.gameObject.SetActive(false);
            Vida3.gameObject.SetActive(false);
        }
        else if (vidas == 0)
        {
            GameBoss.GameOver();
            GameOverAudio.Play();
            Vida1.gameObject.SetActive(false);
            Vida2.gameObject.SetActive(false);
            Vida3.gameObject.SetActive(false);
        }
        else
        {
            GameOverAudio.Play();
            GameBoss.GameOver();
            Vida1.gameObject.SetActive(false);
            Vida2.gameObject.SetActive(false);
            Vida3.gameObject.SetActive(false);
        }
    }

    public void VibrateDevice()
    {
        Handheld.Vibrate();
    }

    void OnCollisionEnter(Collision collision)
    {
        // Comprueba si la colisión involucra al GameObject que quieres desactivar
        if (collision.gameObject.CompareTag("Alien"))
        {
            if (Escudo == true)
            {
                EscudoImage.gameObject.SetActive(false);
                Escudo = false;
                VibrateDevice();
            }
            else
            {
                vidas -= 1;
                VibrateDevice();
                HeridaStatus.EstaHeridoVerdadero();
                Instantiate(explosionAlienPrefab, collision.transform.position, Quaternion.identity); 
            }
        }

        if (collision.gameObject.CompareTag("Estrella"))
        {
            vidas = 3;
            EstrellaPowerup.Play();
            Instantiate(explosionEstrellaPrefab, collision.transform.position, Quaternion.identity); 
        }

        if (collision.gameObject.CompareTag("Asteroide"))
        {
            if (Escudo == true)
            {
                EscudoImage.gameObject.SetActive(false);
                Escudo = false;
                AsteroideClash.Play();
                VibrateDevice();
            }
            else
            {
                vidas -= 2;
                AsteroideClash.Play();
                VibrateDevice();
                HeridaStatus.EstaHeridoVerdadero();
                Instantiate(explosionAsteroidePrefab, collision.transform.position, Quaternion.identity); 
            }
        }

        if (collision.gameObject.CompareTag("BalaEnemigo"))
        {
            if (Escudo == true)
            {
                vidas -= 1;
                Escudo = false;
                EscudoImage.gameObject.SetActive(false);
                VibrateDevice();
            }
            else
            {
                vidas -= 1;
                AsteroideClash.Play();
                VibrateDevice();
                HeridaStatus.EstaHeridoVerdadero();
                Instantiate(explosionBalaEnemigoPrefab, collision.transform.position, Quaternion.identity); 
            }
            SonidoNaveEnemiga.Play();
        }

        if (collision.gameObject.CompareTag("Satelite"))
        {
            Game_Hud.distance += 50;
            SonidoSatelite.Play();
            Instantiate(explosionSatelitePrefab, collision.transform.position, Quaternion.identity); 
        }

        if (collision.gameObject.CompareTag("Super Asteroide"))
        {
            EscudoImage.gameObject.SetActive(false);
            Escudo = false;
            vidas -= 3;
            AsteroideClash.Play();
            VibrateDevice();
            HeridaStatus.EstaHeridoVerdadero();
            Instantiate(explosionSuperAsteroidePrefab, collision.transform.position, Quaternion.identity); 
        }

        if (collision.gameObject.CompareTag("Omega Asteroide"))
        {
            if (Escudo == true)
            {
                EscudoImage.gameObject.SetActive(false);
                Escudo = false;
                AsteroideClash.Play();
                VibrateDevice();
            }
            else
            {
                vidas -= 1;
                AsteroideClash.Play();
                VibrateDevice();
                HeridaStatus.EstaHeridoVerdadero();
                Instantiate(explosionOmegaAsteroidePrefab, collision.transform.position, Quaternion.identity); 
            }
        }

        if (collision.gameObject.CompareTag("Crystal"))
        {
            Escudo = true;
            EscudoImage.gameObject.SetActive(true);
            Shieldup.Play();
            Instantiate(explosionCrystalPrefab, collision.transform.position, Quaternion.identity); 
        }

        // Desactiva el objeto colisionado
        if (collision.gameObject.CompareTag("Crystal") || collision.gameObject.CompareTag("Asteroide") ||
            collision.gameObject.CompareTag("Estrella") || collision.gameObject.CompareTag("Alien") ||
            collision.gameObject.CompareTag("BalaEnemigo") || collision.gameObject.CompareTag("Satelite") ||
            collision.gameObject.CompareTag("Super Asteroide"))
        {
            collision.gameObject.SetActive(false);
        }
    }
}

