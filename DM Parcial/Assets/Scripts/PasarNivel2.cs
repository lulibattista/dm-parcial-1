using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PasarNivel2 : MonoBehaviour
{
    public GameObject LevelDoneScreen;
    public AudioSource MusicaFondo;

    // Start is called before the first frame update
    void Start()
    {
        LevelDoneScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("" + Game_Hud.distance);
        if (Game_Hud.distance >= 100)
        {
            Time.timeScale = 0;
            LevelDoneScreen.SetActive(true);
            MusicaFondo.Stop();
        }
    }
}
