using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiltMovement : MonoBehaviour
{
    public float speed = 10.0f;  // Velocidad de la nave espacial
    public float tiltSensitivity = 2.0f;  // Sensibilidad de la inclinación
    private Vector3 calibrationOffset;

    void Start()
    {
        //CalibrateAccelerometer();
    }

    void Update()
    {
        // Obtén la entrada del acelerómetro
        Vector3 tilt = Input.acceleration - calibrationOffset;

        // Calcula el movimiento en el eje X y Z basado en la inclinación
        float moveHorizontal = tilt.x * tiltSensitivity;
        float moveVertical = tilt.y * tiltSensitivity;

        // Aplica el movimiento a la nave espacial
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        transform.Translate(movement * speed * Time.deltaTime, Space.World);
    }

    // Calibrar el acelerómetro para evitar sesgo inicial
    /*void CalibrateAccelerometer()
    {
        calibrationOffset = Input.acceleration;
    }*/

    // Método para recalibrar el acelerómetro manualmente (opcional)
    /*public void Recalibrate()
    {
        CalibrateAccelerometer();
    }*/
}
