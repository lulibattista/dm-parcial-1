using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PasarNivel3 : MonoBehaviour
{
    public GameObject LevelDoneScreen;
    public AudioSource MusicaFondo;
    // Start is called before the first frame update
    void Start()
    {
        LevelDoneScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Game_Hud.distance >= 150)
        {
            Time.timeScale = 0;
            LevelDoneScreen.SetActive(true);
            MusicaFondo.Stop();
        }
    }
}
