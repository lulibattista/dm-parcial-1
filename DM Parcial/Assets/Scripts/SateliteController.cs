using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SateliteController : MonoBehaviour
{
    public static float SateliteVelocity;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //transform.Translate(SateliteVelocity * Time.deltaTime, 0f, 0f);
        transform.Translate(0f, 0f, -SateliteVelocity * Time.deltaTime);
    }
}