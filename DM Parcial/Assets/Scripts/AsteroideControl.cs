using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroideControl : MonoBehaviour
{
    public static float AsteroideVelocity;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0f, 0f, -AsteroideVelocity * Time.deltaTime);
    }
}
