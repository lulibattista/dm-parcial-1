using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour
{
    public AudioSource BotonSonido;
    public AudioSource BotonSalirSonido;
    public AudioSource BotonAtrasSonido;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayGame()
    {
        BotonSonido.Play();
        SceneManager.LoadScene("Nivel 1");
    }


    public void QuitGame()
    {
        BotonSalirSonido.Play();
        Application.Quit();
        Debug.Log("Saliste del juego");

    }

    public void BotonActivacion()
    {
        BotonSonido.Play();
    }

    public void BotonAtras()
    {
        BotonAtrasSonido.Play();
    }

    public void PlayGame2()
    {
        BotonSonido.Play();
        SceneManager.LoadScene("Nivel 2");
    }

    public void PlayGame3()
    {
        BotonSonido.Play();
        SceneManager.LoadScene("Nivel 3");
    }

    public void AbrirMenu()
    {
        BotonAtrasSonido.Play();
        SceneManager.LoadScene("Menu");
    }

    public void AbrirMenuControl()
    {
        BotonAtrasSonido.Play();
        SceneManager.LoadScene("Como Jugar");
    }
}
