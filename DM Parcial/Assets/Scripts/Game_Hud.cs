using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Game_Hud : MonoBehaviour
{
    public bool gameOver;
    public static float distance = 0;
    public static int DistanciaRedondeada;
    public TMP_Text distanceText;
    public TMP_Text distanceTitle;
    public TMP_Text EnddistanceText;
    public GameObject EndScreen;
    public GameObject PausaScreen;
    public AudioSource MusicaFondo;
    


    // Start is called before the first frame update
    void Start()
    {
        EndScreen.gameObject.SetActive(false);
        gameOver = false;
        distance = 0;
        DistanciaRedondeada = Mathf.RoundToInt(distance);
        distanceText.text = DistanciaRedondeada.ToString() + " km";
        PausaScreen.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver)
        {
            Time.timeScale = 0;
            EndScreen.gameObject.SetActive(true);
            EnddistanceText.text = " " + DistanciaRedondeada + " km";
            distanceText.gameObject.SetActive(false);
            distanceTitle.gameObject.SetActive(false);
            MusicaFondo.Stop();
            


        }
        else
        {
            distance += Time.deltaTime;
            DistanciaRedondeada = Mathf.RoundToInt(distance);
            distanceText.text = DistanciaRedondeada.ToString() + " km";
        }
    }

   
    public void GameOver()
    {
        gameOver = true;
    }
    
    public void PausaMenu()
    {
        Time.timeScale = 0;
        MusicaFondo.Stop();
        PausaScreen.gameObject.SetActive(true);
    }

    public void ReundarJuego()
    {
        Time.timeScale = 1f;
        MusicaFondo.Play();
        
    }

}
