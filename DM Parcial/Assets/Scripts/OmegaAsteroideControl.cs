using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OmegaAsteroideControl : MonoBehaviour
{
    public AudioSource ExplosionAudio;
    public static float OmegaAsteroideVelocity;
    private Rigidbody rb;
    public int VidasAsteroide =2;
    public GameObject OmegaAsteroide;

    public GameObject explosionOmegaAsteroidePrefab;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        OmegaAsteroide.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0f, 0f, -OmegaAsteroideVelocity * Time.deltaTime);
        if (VidasAsteroide <= 0)
        {
            OmegaAsteroide.gameObject.SetActive(false);
            ExplosionAudio.Play();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        // Comprueba si la colisi�n involucra al GameObject que quieres desactivar
        if (collision.gameObject.CompareTag("Bala"))
        {
            // Desactiva el GameObject
            collision.gameObject.SetActive(false);
            ExplosionAudio.Play();
            VidasAsteroide -= 1;
            Instantiate(explosionOmegaAsteroidePrefab, collision.transform.position, Quaternion.identity);

        }

        if (collision.gameObject.CompareTag("Nave"))
        {
            this.gameObject.SetActive(false);
            ExplosionAudio.Play();
            Instantiate(explosionOmegaAsteroidePrefab, collision.transform.position, Quaternion.identity);

        }


    }
}
