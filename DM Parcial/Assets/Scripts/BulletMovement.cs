using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public float speed = 10f; // Velocidad de la bala

    void Update()
    {
        // Mueve la bala hacia adelante en el eje Z global
        transform.Translate(0f, 0f, speed * Time.deltaTime);
    }
}
