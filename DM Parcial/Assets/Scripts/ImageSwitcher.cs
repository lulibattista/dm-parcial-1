using UnityEngine;
using UnityEngine.UI;

public class ImageSwitcher : MonoBehaviour
{
    public GameObject[] images; // Array de im�genes
    private int currentIndex = 0; // �ndice actual de la imagen

    public Button leftButton; // Bot�n izquierdo
    public Button rightButton; // Bot�n derecho

    private Vector2 touchStartPos;
    private bool isSwiping = false;

    void Start()
    {
        // Asegurarse de que solo la primera imagen est� activa al inicio
        for (int i = 0; i < images.Length; i++)
        {
            images[i].SetActive(i == 0);
        }

        // Agregar listeners a los botones
        leftButton.onClick.AddListener(ShowPreviousImage);
        rightButton.onClick.AddListener(ShowNextImage);
    }

    void Update()
    {
        HandleTouchInput();
    }

    void HandleTouchInput()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    touchStartPos = touch.position;
                    isSwiping = true;
                    break;
                case TouchPhase.Moved:
                    if (isSwiping)
                    {
                        float swipeDelta = touch.position.x - touchStartPos.x;

                        if (Mathf.Abs(swipeDelta) > 50)
                        {
                            if (swipeDelta > 0)
                            {
                                ShowPreviousImage();
                            }
                            else
                            {
                                ShowNextImage();
                            }
                            isSwiping = false;
                        }
                    }
                    break;
                case TouchPhase.Ended:
                case TouchPhase.Canceled:
                    isSwiping = false;
                    break;
            }
        }
    }

    void ShowPreviousImage()
    {
        // Desactivar la imagen actual
        images[currentIndex].SetActive(false);

        // Decrementar el �ndice con un manejo seguro
        currentIndex = (currentIndex - 1 + images.Length) % images.Length;

        // Activar la nueva imagen
        images[currentIndex].SetActive(true);
    }

    void ShowNextImage()
    {
        // Desactivar la imagen actual
        images[currentIndex].SetActive(false);

        // Incrementar el �ndice con un manejo seguro
        currentIndex = (currentIndex + 1) % images.Length;

        // Activar la nueva imagen
        images[currentIndex].SetActive(true);
    }
}


