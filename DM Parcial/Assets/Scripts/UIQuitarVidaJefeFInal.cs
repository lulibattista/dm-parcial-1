using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIQuitarVidaJefeFInal : MonoBehaviour
{
    public TMP_Text textoJefeFinal;
    private float r;
    private float g;
    private float b;
    private float a;
    public static bool LastimandoJefe;
    public int VariableContador;
    // Start is called before the first frame update
    void Start()
    {
        r = textoJefeFinal.color.r;
        g = textoJefeFinal.color.g;
        b = textoJefeFinal.color.b;
        a = textoJefeFinal.color.a;
        LastimandoJefe = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (LastimandoJefe)
        {
            a += 0.1f;
            VariableContador -= 1;
        }
        a -= 0.01f;
        a = Mathf.Clamp(a, 0, 1f);
        ChangeColor();
        if (VariableContador <= 0)
        {
            LastimandoJefe = false;
        }
    }

    private void ChangeColor()
    {
        Color c = new Color(r, g, b, a);
        textoJefeFinal.color = c;
    }

    public void EstaHeridoVerdadero()
    {
        LastimandoJefe = true;
        VariableContador = 30;
    }
}
