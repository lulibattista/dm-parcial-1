using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPoolNave : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float bulletSpeed = 10f; // Velocidad de la bala

    private List<GameObject> bulletPool;

    void Start()
    {
        InitializePool();
    }


    void InitializePool()
    {
        bulletPool = new List<GameObject>();
    }


    public void ShootFromPool(Vector3 position, Quaternion rotation)
    {
        GameObject bullet = GetBulletFromPool();
        if (bullet == null)
        {
            bullet = Instantiate(bulletPrefab);
            bulletPool.Add(bullet);
        }

        bullet.transform.position = position;
        bullet.transform.rotation = rotation;
        bullet.SetActive(true);

        Rigidbody bulletRigidbody = bullet.GetComponent<Rigidbody>();
        bulletRigidbody.velocity = bullet.transform.forward * bulletSpeed;
    }

    GameObject GetBulletFromPool()
    {
        foreach (GameObject bullet in bulletPool)
        {
            if (!bullet.activeInHierarchy)
            {
                return bullet;
            }
        }
        return null;
    }
}

